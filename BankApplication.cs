using System;
namespace app
{
    class BankAccount
    {
        private double bal;
        private double prevTrans;
        private string customerName;
        private string customerId;
        public BankAccount(string customerName, string customerId)
        {
            this.customerName = customerName;
            this.customerId = customerId;
        }
        public void Deposit(double amount)
        {
            if (amount != 0)
            {
                this.bal += amount;
                this.prevTrans = amount;
            }
        }
        public void Withdraw(double amt)
        {
            if (amt != 0 && this.bal >= amt)
            {
                this.bal -= amt;
                this.prevTrans = -amt;
            }
            else if (this.bal < amt)
            {
                Console.WriteLine("Bank balance insufficient");
            }
        }
        public void GetPreviousTrans()
        {
            if (this.prevTrans > 0)
            {
                Console.WriteLine("Deposited: " + this.prevTrans);
            }
            else if (this.prevTrans < 0)
            {
                Console.WriteLine("Withdrawn: " + Math.Abs(this.prevTrans));
            }
            else
            {
                Console.WriteLine("No transaction occurred");
            }
        }
        public void Menu()
        {
            char option;
            var sc = new System.IO.StreamReader(Console.OpenStandardInput());
            Console.WriteLine("Welcome " + this.customerName);
            Console.WriteLine("Your ID:" + this.customerId);
            Console.WriteLine("\n");
            Console.WriteLine("a) Check Balance");
            Console.WriteLine("b) Deposit Amount");
            Console.WriteLine("c) Withdraw Amount");
            Console.WriteLine("d) Previous Transaction");
            Console.WriteLine("e) Exit");
            do
            {
                Console.WriteLine("********************************************");
                Console.WriteLine("Choose an option");
                option = (char)sc.Read();
                Console.WriteLine("\n");
                switch (option)
                {
                    case 'a':
                        Console.WriteLine("......................");
                        Console.WriteLine("Balance =" + this.bal);
                        Console.WriteLine("......................");
                        Console.WriteLine("\n");
                        break;
                    case 'b':
                        Console.WriteLine("......................");
                        Console.WriteLine("Enter a amount to deposit :");
                        Console.WriteLine("......................");
                        double amt = double.Parse(sc.ReadLine());
                        Deposit(amt);
                        Console.WriteLine("\n");
                        break;
                    case 'c':
                        Console.WriteLine("......................");
                        Console.WriteLine("Enter a amount to Withdraw :");
                        Console.WriteLine("......................");
                        double amtW = double.Parse(sc.ReadLine());
                        Withdraw(amtW);
                        Console.WriteLine("\n");
                        break;
                    case 'd':
                        Console.WriteLine("......................");
                        Console.WriteLine("Previous Transaction:");
                        GetPreviousTrans();
                        Console.WriteLine("......................");
                        Console.WriteLine("\n");
                        break;
                    case 'e':
                        Console.WriteLine("......................");
                        break;
                    default:
                        Console.WriteLine("Choose a correct option to proceed");
                        break;
                }
            } while (option != 'e');
            Console.WriteLine("Thank you for using our banking services");
        }
    }
    class BankApplication
    {
        static void Main(string[] args)
        {
            var sc = new System.IO.StreamReader(Console.OpenStandardInput());
            Console.WriteLine("Enter your 'Name' and 'CustomerId' to access your Bank account:");
            string name = sc.ReadLine();
            string customerId = sc.ReadLine();
            BankAccount obj1 = new BankAccount(name, customerId);
            obj1.Menu();
        }
    }
}